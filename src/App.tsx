import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

import Main from './other/screens/main'
import Diagram from './other/screens/chart'

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path='/' component={Main} />
        <Route exact path='/chart' component={Diagram} />
      </Switch>
    </Router>
  )
}

export default App