import React, { FC } from 'react'
import { VictoryHistogram, VictoryChart, VictoryAxis } from 'victory'

import useHistogram from './state'
import './styles.css'

const Histogram: FC = () => {
    const { data } = useHistogram()

    return (
        <div className='__histogram'>
            <h4 className='__metric_title'>Users Activity</h4>
            <VictoryChart
                width={500}
                height={500}
                animate={{ duration: 1000 }}
            >
                <VictoryHistogram 
                    style={{
                        data: {
                            fill: "#eee",
                            stroke: "black",
                            fillOpacity: 1,
                            strokeWidth: 1
                        },
                    }}
                    data={data}
                />
                <VictoryAxis
                    label="Time (days)"
                    style={{
                        axisLabel: { fontSize: 22, padding: 33 },
                        tickLabels: { fontSize: 20, padding: 5 },
                    }}
                />
                <VictoryAxis
                    dependentAxis
                    label="Users"
                    tickFormat={t => t}
                    style={{
                        axisLabel: { fontSize: 22, padding: 33 },
                        tickLabels: { fontSize: 20, padding: 5 },
                    }}
                />
            </VictoryChart>
        </div>
    )
}

export default Histogram