import React, { useState, useEffect } from 'react'
import axios from 'axios'

import { getAPI } from '../../api'
import { IData } from '../../types'

const useHistogram = () => {
    const [ data, setData ] = useState([])

    useEffect(() => {
        axios.get(getAPI).then(res => {
            const histogram = res.data.map((obj: IData) => {
                const reg = new Date(obj.reg_date)
                const last = new Date(obj.last_activity_date)

                return {x: (last.getTime() - reg.getTime()) / (1000 * 3600 * 24)}
            })
            console.log(histogram)
            setData(histogram)
        })
    }, [])

    return { data }
}

export default useHistogram