import React, { FC } from 'react'
import { Link } from 'react-router-dom'

import './styles.css'

const ToolBar: FC = () => {
    return (
        <div className='__table_toolbar'>
            <h3 className='__table_h3'>
                A/B Test
            </h3>
            <Link className='__table_to' to='/chart'>
                Calculate Charts
            </Link>
        </div>
    )
}

export default ToolBar