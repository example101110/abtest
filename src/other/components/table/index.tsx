import React, { FC } from 'react'
import MaterialTable from 'material-table'

import ToolBar from './table-toolbar'
import AddTable from '../addTable'
import useTable from './state'
import tableIcons from './icons'
import './styles.css'


const tableStyles = {
    boxShadow: '4px 0 10px rgba(93, 109, 151, 0.1)',
    border: '1px solid #eee',
    borderRadius: '10px',
    paddingBottom: '30px'
}


const Table: FC = () => {
    const { data, onAdd, loading } = useTable()

    return (
        <div className='__table'>
            <MaterialTable
                components={{
                    Toolbar: props => <ToolBar />
                }}
                icons={tableIcons}
                style={tableStyles}
                options={{
                    draggable: false,
                    search: false,
                    filtering: false,
                }}
                columns={[
                    { title: 'User ID', field: 'id' },
                    { title: 'Date Registration', field: 'reg_date', type: 'date' },
                    { title: 'Date Last Activity', field: 'last_activity_date', type: 'date' },
                ]}
                data={data}
            />
            <AddTable onAdd={onAdd} />
        </div>
    )
}

export default Table