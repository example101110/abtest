import React, { useState, useEffect } from 'react'
import axios from 'axios'

import { getAPI, createAPI } from '../../api'

const useTable = () => {
    const [ loading, setLoading ] = useState(true)
    const [ data, setData ] = useState<object[]>([])
    
    useEffect(() => {

        axios.get(getAPI).then((res) => {
            setData(res.data)
            setLoading(false)
        })

    }, [])

    const onAdd = (e: any, regDate: string, lastActiveDate: string) => {
        e.preventDefault()

        if (new Date(regDate) > new Date(lastActiveDate)) {
            return alert('Please, submit correct data')
        }

        setLoading(true)

        axios.post(createAPI, {
            reg_date: regDate,
            last_activity_date: lastActiveDate
        })
        .then(res => {
            const newItem = {
                id: res.data.id,
                reg_date: res.data.reg_date,
                last_activity_date: res.data.last_activity_date
            }
            
            setData(data => [...data, newItem])
            setLoading(false)
        })
        .catch(() => {
            setLoading(false)
        })
    }

    return { data, setData,  onAdd, loading}
}

export default useTable