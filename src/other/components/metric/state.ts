import React, { useState, useEffect } from 'react'
import axios from 'axios'

import { rollAPI } from '../../api'

const useMetric = () => {
    const [ data, setData ] = useState([])

    useEffect(() => {
        axios.get(rollAPI).then(res => {
            setData(res.data)
        })    
    }, [])
    
    return { data }
}

export default useMetric