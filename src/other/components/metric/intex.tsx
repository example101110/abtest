import React, { FC } from 'react'
import { VictoryChart, VictoryArea, VictoryAxis } from 'victory'

import useMetric from './state'
import './styles.css'

const Metric: FC = () => {
    const { data } = useMetric()

    return (
        <div className='__metric'>
            <h4 className='__metric_title'>Rolling Retention 7 day</h4>
            <VictoryChart
                width={500}
                height={500}
                animate={{ duration: 1000 }}
            >
                <VictoryArea
                    style={{
                        data: {
                            fill: "#eee",
                            stroke: "black",
                            fillOpacity: 1,
                            strokeWidth: 2
                        }}}
                    data={data}
                    interpolation='basis'
                />
                <VictoryAxis
                    tickFormat={t => t}
                    label="Time (days)"
                    style={{
                        axisLabel: { fontSize: 22, padding: 33 },
                        tickLabels: { fontSize: 20, padding: 5 },
                    }}
                />
                <VictoryAxis
                    dependentAxis
                    label="Retention Rate %"
                    style={{
                        axisLabel: { fontSize: 22, padding: 30 },
                        tickLabels: { fontSize: 20, padding: 2 },
                    }}
                />
            </VictoryChart>
        </div>
    )
}

export default Metric