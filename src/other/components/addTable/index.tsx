import React, { FC, useState } from 'react'

import { IAddTable, IAddQuery } from './types'
import './styles.css'

const AddQuery: FC<IAddQuery> = ({set, value, txt}) => {
    return (
        <div className='__add_query'>
            <label>{txt}</label>
            <input className='__add_input' type='date' value={value} onChange={e => set(e.target.value)} />
        </div>
    )
}

const AddTable: FC<IAddTable> = ({ onAdd, loading }) => {
    const [ regDate, setRegDate ] = useState('')
    const [ lastActiveDate, setLastActiveDate ] = useState('')

    return (
        <form className='__add' onSubmit={(e) => onAdd(e, regDate, lastActiveDate)}>
            <h3 className='__add_title'>
                Change
            </h3>
            <AddQuery txt='Registered Date' value={regDate}  set={setRegDate} />
            <AddQuery txt='Last Active Date' value={lastActiveDate}  set={setLastActiveDate} />
            <input className='__add_button' type='submit' value='Add new user' />
        </form>
    )
}

export default AddTable