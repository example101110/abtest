export interface IAddTable {
    onAdd: (event: any, reg: string, last: string) => void,
    loading?: boolean
}

export interface IAddQuery {
    value: string,
    set: (value: string) => void,
    txt: string
}