import React, { FC } from 'react'
import Table from '../components/table'
import { Link } from 'react-router-dom'


const Main: FC = props => {
    return (
        <div>
            <Table />
        </div>
    )
}

export default Main