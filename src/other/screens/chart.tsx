import React, { FC } from 'react'
import { Link } from 'react-router-dom'

import Histogram from '../components/histogram'
import Metric from '../components/metric/intex'
import './styles.css'

const Chart: FC = () => {
    return (
        <div className='__chart'>
            <div className='__chart_flex'>
                <Histogram />
                <Metric />
            </div>
            <Link className='__chart_button' to='/'>
                Show Table
            </Link>
        </div>
    )
}

export default Chart