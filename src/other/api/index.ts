export const API = 'https://safe-crag-52283.herokuapp.com/'
export const createAPI = API + 'api/add/'
export const getAPI = API + 'api/get/'
export const rollAPI = API + 'api/rolling/'