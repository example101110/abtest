export interface IData {
    id: number,
    reg_date: string,
    last_activity_date: string
}